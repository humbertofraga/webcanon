import threading
import time

import gi
import gphoto2
import v4l2ctl
import cairo

gi.require_version("Gtk", "3.0")
gi.require_version("Gst", "1.0")
gi.require_version('GstVideo', '1.0')
from gi.repository import Gtk
from gi.repository import Gst
from gi.repository.Gst import Pipeline
from gi.repository import GstVideo, GdkX11

from subprocess import Popen, PIPE
from os import path, mkfifo, remove


class Main:
    def __init__(self):
        self.playing = False
        self.capture_thread = None
        self.device = self.get_v4l2loopback_device()

        self.builder = Gtk.Builder()
        self.builder.add_from_file(path.join(path.abspath(path.dirname(__file__)), "main.glade"))
        self.combo = self.builder.get_object("combo_sources")
        renderer_text = Gtk.CellRendererText()
        self.combo.pack_start(renderer_text, True)
        self.combo.add_attribute(renderer_text, "text", 1)
        self.store = Gtk.ListStore(int, str)
        self.combo.set_model(self.store)

        self.capture_pipeline = Gst.Pipeline()
        preview_source = Gst.parse_launch("v4l2src device={}".format(self.device))
        preview_sink = Gst.ElementFactory.make("glimagesink", None)
        self.capture_pipeline.add(preview_source)
        self.capture_pipeline.add(preview_sink)
        preview_source.link(preview_sink)

        bus = self.capture_pipeline.get_bus()
        bus.add_signal_watch()
        bus.enable_sync_message_emission()
        bus.connect("message", self.on_message)
        bus.connect("sync-message::element", self.on_sync_message)

        self.preview = self.builder.get_object("video_preview")

        handlers = {
            "onDestroy": self.quit,
            "onUpdateClicked": self.load_sources,
            "onPlayPause": self.on_play_pause
        }
        self.builder.connect_signals(handlers)

        self.status = self.builder.get_object("status")

        window = self.builder.get_object("window")
        window.show_all()
        window.realize()
        self.load_sources(None)

    def message(self, text):
        ctx = self.status.get_context_id("messages")
        self.status.push(ctx, text)

    def quit(self, arg):
        self.playing = False
        self.capture_pipeline.set_state(Gst.State.NULL)
        if self.capture_thread:
            self.capture_thread.join()
        Gtk.main_quit()

    def get_v4l2loopback_device(self):
        devices = v4l2ctl.v4l2device.V4l2Device.iter_devices()
        for device in devices:
            if device.driver == 'v4l2 loopback' or device.driver == 'dslr-webcam':
                return str(device.device)

    def load_sources(self, button):
        self.store.clear()
        cameras = gphoto2.Camera.autodetect()
        for n, (name, usb) in enumerate(cameras):
            print("{}: {}".format(n, name))
            self.store.append([n, str(name)])
        if len(self.store) > 0:
            self.combo.set_active(0)
            self.message("Pronto")
        else:
            self.message("Nenhuma câmera detectada. Ela está ligada?")

    def on_play_pause(self, button):
        if not self.playing:
            # Prepare webcam from gphoto2
            self.message("Preparando câmera")
            camera_list = list(gphoto2.Camera.autodetect())
            if len(camera_list) == 0:
                self.message("Nenhuma câmera detectada. Ela está ligada?")
                return False
            name, addr = camera_list[self.combo.get_active()]
            camera = gphoto2.Camera()
            port_info_list = gphoto2.PortInfoList()
            port_info_list.load()
            idx = port_info_list.lookup_path(addr)
            camera.set_port_info(port_info_list[idx])
            camera.init()
            # Start ffmpeg capture to video device
            self.message("Preparando captura")
            gst_capture = Popen(['gst-launch-1.0', 'fdsrc', '!', 'decodebin', '!', 'videoconvert',
                                 '!', 'v4l2sink', "device={}".format(self.device)],
                                stdin=PIPE)
            self.capture_thread = threading.Thread(target=self.capture_loop, args=(camera, gst_capture))
            self.message("Iniciando captura")
            self.capture_thread.start()
            self.playing = True
            icon = self.builder.get_object("icon_stop")
            button.set_image(icon)

            # Starts gstreamer preview
            pipeline_thread = threading.Thread(target=self.start_pipeline)
            pipeline_thread.start()
            print("Starting pipeline")
            self.message("Carregando...")
        else:
            self.capture_pipeline.set_state(Gst.State.NULL)
            self.playing = False
            icon = self.builder.get_object("icon_play")
            button.set_image(icon)

    def start_pipeline(self):
        time.sleep(3)
        self.capture_pipeline.set_state(Gst.State.PLAYING)

    def capture_loop(self, camera, process):
        print("Starting capture")
        self.message("Captura iniciada")
        while self.playing:
            capture = camera.capture_preview()
            filedata = capture.get_data_and_size()
            data = memoryview(filedata)
            process.stdin.write(data.tobytes())
        print("Stopping capture")
        self.message("Parando captura")
        process.terminate()
        camera.exit()
        self.capture_pipeline.set_state(Gst.State.NULL)
        self.message("Pronto")

    def on_message(self, bus, message):
        t = message.type
        if t == Gst.MessageType.EOS:
            self.capture_pipeline.set_state(Gst.State.NULL)
            self.playing = False
            self.message("Pronto")
        elif t == Gst.MessageType.ERROR:
            err, debug = message.parse_error()
            print("Error: {}".format(err))

    def on_sync_message(self, bus, message):
        self.message("Exibindo")
        if message.get_structure().get_name() == 'prepare-window-handle':
            sink = message.src
            sink.set_window_handle(self.preview.get_property("window").get_xid())


def main():
    Gst.init(None)
    Main()
    Gtk.main()


if __name__ == '__main__':
    main()
