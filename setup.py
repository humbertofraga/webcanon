import setuptools
import webcanon

setuptools.setup(
    name=webcanon.__name__,
    version=webcanon.__version__,
    author="Humberto Fraga",
    author_email="humbertofraga@gmail.com",
    url="",
    packages=setuptools.find_packages(),
    package_dir={"webcanon": "webcanon"},
    package_data={"webcanon": ["main.glade"]},
    data_files=[
        ('share/applications', ['webcanon.desktop']),
    ],
    keywords="video",
    entry_points={
        "console_scripts": ["webcanon=webcanon.main:main"]
    },
    install_requires=[i.strip() for i in open("requirements.txt").readlines()],
    python_requires='>=3.0'
)
